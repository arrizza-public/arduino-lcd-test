#ifndef DISPLAYBUFFER_H_
#define DISPLAYBUFFER_H_

// manages the an internal display buffer
class DisplayBuffer
  {
  public:
    // ctor
    DisplayBuffer();

    // clear the screen
    void clear();

    // send the buffer contents to the LCD
    void send();

    // sound the beeper
    void beep();

    // turn on the LED
    void ledOn();

    // turn off the LED
    void ledOff();

    // set the line to the given buffer and fill the rest, if any, with spaces
    void setLine1(const char* buf);
    void setLine2(const char* buf);

  private:
    void setLine(char* line, const char* buf);

  private:
    char mLine1[16];
    char mLine2[16];
  };

#endif /* DISPLAYBUFFER_H_ */

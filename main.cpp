#include "DisplayBuffer.h"
#include "WProgram.h"

// the LCD buffer
static DisplayBuffer db;

//===========================
void setup()
  {
  // LCD communicates at 4800
  Serial.begin(4800);

  // clear the screen
  db.clear();

  // set the first line to a sample value
  db.setLine1("john was here.");
  // check what happens if lines are too long
  //           1234567890123456
  db.setLine2("hello world 1234567890");

  // send the buffer contents to the LCD
  db.send();

  // make sure the user can see it for a few seconds
  delay(5000);

  // clear the screen again
  db.clear();
  }

//===========================
void loop()
  {
  // keep track of a counter so we can see it change
  static int count = 0;

  // keep track of the led state
  static bool ledstate = false;

  // keep count. Every so often, sound the beeper
  count++;
  if (count >= 100)
    {
    count = 0;
    db.beep();
    }

  // flash the led
  ledstate = !ledstate;
  if (ledstate)
    {
    db.ledOn();
    }
  else
    {
    db.ledOff();
    }

  // delay, so the user can see it
  delay(250);

  // check if the user pressed a button
  char buf[15];
  if (Serial.available())
    {
    // the user pressed a button, so go get it
    char key = Serial.read();

    // the value is ascii '1' thru '6'
    sprintf(buf, "key %c", key);

    // display what they pressed
    db.setLine1(buf);
    }
  else
    {
    // the user didn't press a button
    // so just display the current count
    sprintf(buf, "Count:%5d", count);
    db.setLine2(buf);
    }

  // show the buffer contents on the LCD
  db.send();
  }

//===========================
int main()
  {
  init();
  setup();
  for (;;)
    {
    loop();
    }
  return 0;
  }
